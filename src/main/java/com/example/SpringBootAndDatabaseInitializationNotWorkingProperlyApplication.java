package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.HashMap;

@SpringBootApplication
public class SpringBootAndDatabaseInitializationNotWorkingProperlyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAndDatabaseInitializationNotWorkingProperlyApplication.class, args);
	}

	@Configuration
	@EnableJpaRepositories(
			basePackages = "com.example",
			entityManagerFactoryRef = "userEntityManager",
			transactionManagerRef = "userTransactionManager"
	)
	public class DataBaseConfig {


		@Bean
		@Primary
		@ConfigurationProperties("app.datasource.foo")
		public DataSourceProperties fooDataSourceProperties() {
			return new DataSourceProperties();
		}

		@Bean
		@Primary
		@ConfigurationProperties("app.datasource.foo")
		public DataSource fooDataSource() {
			return fooDataSourceProperties().initializeDataSourceBuilder().build();
		}

		@Bean
		@ConfigurationProperties("app.datasource.bar")
		public DataSourceProperties barDataSourceProperties() {
			return new DataSourceProperties();
		}

		@Bean
		@ConfigurationProperties("app.datasource.bar")
		public DataSource barDataSource() {
			return barDataSourceProperties().initializeDataSourceBuilder().build();
		}
	}

}
